#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#define PORT 8080
#define SIZE 100
#define BUFF 1024
int sock = 0, valread, auth_ = 0;
char userlog[SIZE]={};

void read2d(char arr[][SIZE], int idx){

    for (int i=0; i<idx; i++){
        read(sock, arr[i], sizeof(arr[i]));
    }

}

void write2d(char arr[][SIZE], int idx){

    for (int i=0; i<idx; i++){
        write(sock, arr[i], sizeof(arr[i]));
    }

}

int string_to_int(char s[]){

    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;

}

// Login
void login(){

    int status;
    char id[BUFF], password[BUFF], temp_buff[BUFF]={};
    char id_data[BUFF][SIZE], pwd_data[BUFF][SIZE];
    int idx;
    valread = read(sock, temp_buff, BUFF);
    idx = string_to_int(temp_buff);
    read2d(id_data, idx);
    read2d(pwd_data, idx);
  
    printf("Server free, you may type now\n");
    while(!auth_){
        int auth_idx=-1;
        while(auth_idx == -1){
            
            // Dapatkan input id/username
            printf("Username: ");
            scanf("%s", id);

            // Cek apakah id/username sudah terdaftar
            for (int i = 0; i < idx; i++){

                if (!strcmp(id_data[i], id)){
                    auth_idx = i;
                    break;
                }
            }

            if(auth_idx == -1)
            {
                printf("Wrong Username\n");
            }

        }

        // Dapatkan input password
        printf("Password: ");
        scanf("%s", password);

        // Cek apakah password sudah terdaftar
        if (!strcmp(password, pwd_data[auth_idx])){
            strcpy(userlog, id_data[auth_idx]);
            auth_ = 1;
            printf("Login Successful\n");
        }else {
            printf("Wrong Password\n");
        }
    }
    
}

// Register
void regist(){
    
    bool exist=true;
    char id[SIZE]={}, password[SIZE]={}, temp_buff[BUFF]={};
    char id_data[BUFF][SIZE]={};
    int idx, i;
    valread = read(sock, temp_buff, BUFF);
    idx = string_to_int(temp_buff);
    valread = read(sock, id_data, sizeof(id_data));
    printf("Server free, you may type now\n");

    // Mengambil input username
    printf("Username: ");
    while(exist){
        scanf("%s", id);

        // Mengecek apakah username sudah ada
        for (i = 0; i < idx; i++){
            if (!strcmp(id_data[i], id)){
                exist=false;
                break;
            }
        }
        if (!exist){
            exist = true;
            printf("Username already exist\nUsername: ");
        }else {
            exist = false;
            getchar();
        }
    }

    // Mengambil input password
    printf("Password: ");
    int nc, lc, ucc, lcc;
    nc=lc=ucc=lcc=0;
    while(!lc || !nc || !ucc || !lcc)
    {
        scanf("%s", password);
        int len = strlen(password);

        // Mengecek apakah password memenuhi syarat
        for(int i=0; i<len; i++)
        {
            if(isdigit(password[i]) && nc==0)
            {
                nc=1;
            }

            if(isupper(password[i]) && ucc==0)
            {
                ucc=1;
            }

            if(islower(password[i]) && lcc==0)
            {
                lcc=1;
            }

            if(len>=6 && lc==0)
            {
                lc=1;
            }
        }

        if(!lc || !nc || !ucc || !lcc)
        {
            printf("\nPassword Format Invalid, password must contain the following:\n");
            if (!lc) printf("- Minimum 6 characters\n");
            if (!nc) printf("- A  number\n");
            if (!lcc) printf("- A  lowercase letter\n");
            if (!ucc) printf("- An uppercase letter\n");
            nc=lc=ucc=lcc=0;
            memset(password, '\0', SIZE);
            printf("Password: ");
        }

    }
  
    write(sock, id, BUFF);
    write(sock, password, BUFF);
}

// Memilih antara register dan login
void reg_log(){

    char chc[SIZE];
    printf("Enter login/register: ");
    scanf("%s", chc);
    printf("Dont type anything, wait until another client disconnect!\n");
    write(sock, chc, BUFF);
    if(!strcmp(chc, "register"))
    {
       regist();
    }
    else if(!strcmp(chc, "login"))
    {
       login();
    }

}  

void writeInt(int var, int size){

    char buffer[BUFF]={};
    sprintf(buffer, "%d", var);
    write(sock, buffer, size);

}

int command(){

    char com[SIZE]={};
    printf("Command list\nadd\nsee\nexit\nCommand: ");
    scanf("%s", com);
    write(sock, com, sizeof(com));

    if (!strcmp(com, "add")){

        char question[4][SIZE]={0};
        char client_dir[SIZE*2];
        getcwd(client_dir, SIZE*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[SIZE]={}, path[SIZE]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[SIZE]={};
                read(sock, temp, SIZE);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, SIZE);
                }
            }
        }
        printf("\n");
        return 1;

    }
    
    else if (!strcmp(com, "see")){

        char author[SIZE], problem[SIZE], buffer[SIZE];
        printf("Problem list:\n");
        valread = read(sock, buffer, SIZE);
        int idx = string_to_int(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, SIZE);
            read(sock, author, SIZE);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;

    }
    
    else if (!strcmp(com, "exit")){
        return 0;
    }else {
        printf("Command %s not available\n\n", com);
        return 1;
    }
}

int main(int argc, char const *argv[]) {

    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[BUFF] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    reg_log();
    writeInt(auth_, BUFF);
    write(sock, userlog, sizeof(userlog));
    if(auth_){
        while(command());
    }
    close(sock);
    return 0;

}
