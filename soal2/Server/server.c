#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h> 
#define PORT 8080
#define BUFF 1024
#define SIZE 100

int valread;
char *users_file = "users.txt", *database = "problems.tsv";
char userlog[SIZE]={};

void download_file(char *src_path, char *dst_path){

    int src_fd, dst_fd, n, err;
    unsigned char buffer[BUFF*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, BUFF*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);

}

void write2d(char arr[][SIZE], int idx, int new_socket){

    for (int i=0; i<idx; i++){
        write(new_socket, arr[i], sizeof(arr[i]));
    }

}

int string_to_int(char s[], int new_socket){

    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;

}

// Login
void login_handler(int new_socket){

    // Membuka file users.txt dengan mode r+
    FILE* fptr = fopen(users_file,"r+");

    // Memasukkan data id dan password ke dalam array
   char id_data[BUFF][SIZE], pwd_data[BUFF][SIZE];
   int idx=0;
    while(!feof(fptr)) {
       fscanf(fptr, "%[^:]:%[^\n]\n", id_data[idx], pwd_data[idx]);
       idx++;
   } 

    fclose(fptr);
    char temp_buff[BUFF]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, BUFF);
    write2d(id_data, idx, new_socket);
    write2d(pwd_data, idx, new_socket);

}

// Register
void regist_handler(int new_socket){

    /* Membuka file users.txt dengan mode a+
     * Supaya bisa melakukan read, write, append dan membuat file baru jika memang belum ada */
    FILE* fptr = fopen(users_file,"a+");
    
    // Memasukkan data id/username dari file users.txt ke dalam array 
    char line[BUFF], id_data[BUFF][SIZE]={}, pwd_data[BUFF][SIZE];
    int idx=0;
   while(!feof(fptr)) {
         fscanf(fptr, "%[^:]:%[^\n]\n", id_data[idx], pwd_data[idx]);
        idx++;
   }
    char temp_buff[BUFF]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, BUFF);
    write(new_socket, id_data, sizeof(id_data));

    // Membaca data id dan password dari client lalu dimasukkan ke file users.txt
    char id[BUFF]={}, 
         pass[BUFF]={};
    valread = read(new_socket, id, BUFF);
    valread = read(new_socket, pass, BUFF);
    fprintf(fptr, "%s:%s\n", id, pass);
    printf("User '%s' successfully registered\n", id);
    fclose(fptr);
    
}

// Memilih antara register dan login
void reg_log_handler(int new_socket){

   char chc[BUFF];
   valread = read(new_socket, chc, BUFF);
   if(!strcmp(chc, "register"))
   {
       regist_handler(new_socket);
   }
   else if(!strcmp(chc, "login"))
   {
       login_handler(new_socket);
   }

}

void writeInt(int var, int size, int new_socket){

    char buffer[SIZE]={};
    sprintf(buffer, "%d", var);
    write(new_socket, buffer, size);

}

void copyToken(char* destination,
               char* source,
               size_t maxLen,
               char const* delimiter)
{
    char* token = strtok(source, delimiter);
    if ( token != NULL )
    {
       destination[0] = '\0';
       strncat(destination, token, maxLen-1);
    }
}


int command_handler(int new_socket){
    
    char com[SIZE]={};
    valread = read(new_socket, com, sizeof(com));

    if (!strcmp(com, "add")){

        printf("User '%s' adding a problem\n", userlog);
        FILE* fp = fopen(database, "a+");

        char question[4][SIZE] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);

        char serv_path[SIZE*2], client_path[SIZE*2], prob[SIZE]={};
        read(new_socket, client_path, sizeof(client_path));

        for(int i=0; i<4; i++){
            char buffer[BUFF]={}, src_path[BUFF]={}, dest_path[BUFF]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", SIZE);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", SIZE);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                download_file(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem '%s' has been added by '%s'\n", prob, userlog);
        fclose(fp);
        return 1;

    }

    else if (!strcmp(com, "see"))
    {

        printf("User '%s' see problem list\n", userlog);
        FILE* fp = fopen(database, "r+");
        char author[BUFF][SIZE]={}, problem[BUFF][SIZE]={};

        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, SIZE, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], SIZE);
            write(new_socket, author[i], SIZE);
        }
        fclose(fp);
        return 1;
        
    }
    
    return 0;

} 

void *client_connect_handler(void *sock){

    int new_socket = *(int*)sock;
    int auth_ = 0;
    char buffer[BUFF] = {};
    reg_log_handler(new_socket);
    valread = read(new_socket, buffer, BUFF);
    auth_ = string_to_int(buffer, new_socket);
    valread = read(new_socket, userlog, sizeof(userlog));
    if (auth_){
        printf("User '%s' logged in\n", userlog);
        while(command_handler(new_socket));
    }
    free(sock);

}

int main(int argc, char const *argv[]) {
    int server_fd;
    struct sockaddr_in address;
    int opt = 1, new_socket;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))< 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for client connections...\n");
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");
        FILE* fp_db = fopen(database, "a+");
        FILE* fp_user = fopen(users_file, "a+"); 
        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0)
		{
			perror("could not create thread");
			return 1;
		}
		
		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    if (new_socket < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    return 0;
}
