# LAPORAN RESMI SOAL SHIFT MODUL 3

# Kelompok A06

## Anggota Kelompok

<ul>
    <li>Hilmi Zharfan Rachmadi (5025201268)</li>
    <li>Andhika Ditya Bagaskara D. (5025201096)</li>
    <li>Muhammad Ismail (5025201223)</li>
</ul> 

## Soal no. 1
Pada soal nomor 1, karena akan menggunakan thread, maka dideklarasikan:

```bash
pthread_t tid[3];
pid_t child;
int stats = 0;
```
Untuk soal 1a, pertama-tama kita diminta untuk mendownload file zip dari link:

https://drive.google.com/file/d/1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt/view?usp=sharing untuk ```quote.zip``` dan

https://drive.google.com/file/d/1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1/view?usp=sharing untuk ```music.zip```.

Untuk melakukan download, digunakan fungsi ```dl()``` sebagai berikut:

```bash
void* dl(void *arg)
{
    char *argv1[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "/home/kadigas/modul3/quote.zip", NULL};
    char *argv2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "/home/kadigas/modul3/music.zip", NULL};
	
    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/wget", argv1);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1]))
	{
        child = fork();
        if (child==0) {
		    execv("/usr/bin/wget", argv2);
	    }
        else {
		    ((wait(&status)) > 0);
        }
	}

    stats = 1;

	return NULL;
}
```
di mana merupakan fungsi yang dijalankan oleh thread untuk mendownload kedua file secara bersamaan. Agar file yang didownload tidak digunakan sebelum thread selesai berjalan, digunakan variable global ```stats``` untuk mutual exclusion (mutex). Apabila kedua thread telah berhasil mendownload, maka ```stats``` akan diset = 1. Untuk mendownload, digunakan ```wget``` yang akan dieksekusi fungsi ```execv("/usr/bin/wget", argv)``` dengan menggunakan ```fork()```. Fungsi ```dl()``` ini akan dipanggil pada main:

```bash
int main(void)
{
	int i=0;
	int err;
    while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&dl,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
Variable ```i``` akan berguna untuk indexing dalam loop while sehingga thread yang ada dalam index tersebut dapat memanggil fungsi yang dipassing. Dilakukan loop ```while(i<2)``` sehingga akan dibuat 2 thread untuk menjalankan fungsi ```dl()```. Setelah kedua thread selesai, dilakukan join thread.

Setelah mendownload, maka hal selanjutnya yang dilakukan adalah mengunzip kedua file ```.zip```. ```quote.zip``` akan diunzip ke directory ```./modul3/quote/``` dan ```music.zip``` akan diunzip ke directory  ```./modul3/music/```. Maka dengan menggunakan fungsi ```unzipping()```:

```bash
void* unzipping(void *arg)
{
    while(stats != 1){

    }

	char *argv1[] = {"unzip", "-q", "quote.zip", "-d", "/home/kadigas/modul3/quote", NULL};
	char *argv2[] = {"unzip", "-q", "music.zip", "-d", "/home/kadigas/modul3/music", NULL};
	
	pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/unzip", argv1);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1]))
	{
        child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv2);
	    }
        else {
		    ((wait(&status)) > 0);
        }
	}

    stats = 2;

	return NULL;
}
```
Dengan penggunaan mutex, fungsi ```unzipping()``` akan menunggu hingga ```stats == 1```. Dengan struktur yang mirip dengan ```dl()```, fungsi ```unzip``` kedua file ```.zip``` akan dijalankan oleh kedua thread yang mengeksekusi ```execv("/usr/bin/unzip", argv)``` dengan ```fork()``` ke masing-masing directory destination-nya. Setelah thread selesai mengeksekusi, ```stats``` akan diset = 2. Fungsi ```unzipping()``` dipanggil oleh main:

```bash
    i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipping,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
variable ```i``` akan diset = 0 terlebih dahulu agar loop while akan dimulai dari ```i == 0```.

Untuk soal 1b, kita diminta untuk men-decode file ```.txt``` yang telah diunzip dengan menggunakan base64. Untuk itu, dengan menggunakan fungsi ```dcd()```:

```bash
void* dcd(void *arg)
{
    while(stats != 2){

    }

    char path1[] = "/home/kadigas/modul3/quote/";
	char path2[] = "/home/kadigas/modul3/music/";

    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0]))
	{
		list(path1);
    }

	else if(pthread_equal(id,tid[1]))
	{
        list2(path2);
	}
	
	return NULL;
}
```
Fungsi ```dcd()``` akan menunggu hingga ```stats == 2``` dan akan mengeksekusi ```list()``` dan ```list2()``` masing-masing pada kedua thread. Kedua fungsi tersebut:

```bash
void list(char *basePath){
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul3/quote.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        decode(src, fptr);
      }
    }

    fclose(fptr);
    closedir(dir);
}

void list2(char *basePath){
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul3/music.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        decode(src, fptr);
      }
    }

    fclose(fptr);
    closedir(dir);
}
```
Kedua fungsi ini mirip, hanya berbeda pada file yang diopen, di mana ```list()``` akan membuka ```quote.txt```, sedangkan ```list2()``` akan membuka ```music.txt``` pada directory ```./modul3/```. Dengan menggunakan ```dirent```, dilakukan directory listing pada masing-masing directory yang dipassing (```list()``` = ```./modul3/quote/``` dan ```list2()``` = ```./modul3/music/```). Pada pembacaan, akan dilakukan pengecekan apabila nama file yang kini dibaca (```dp->d_name```) bukan ```.```, ```..```, dan pada substring-nya memiliki ```.txt```. Apabila ketiga kondisi terpenuhi, maka variable ```src``` akan mengcopy ```basePath``` yang dipassing ke masing-masing fungsi, ditambahkan dengan ```dp->d_name``` sekarang (misalkan ```dp->d_name``` = ```q1.txt```, maka ```src``` akan berisi ```/home/kadigas/modul3/quote/q1.txt```), lalu akan dipassing bersama dengan file pointer ```fptr``` untuk mengeksekusi fungsi ```decode()```:

```bash
#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);

void decode(char *path, FILE *fptr){
    int fd[2];
    pid_t pid;
    char decoded[4096];
    
    if (pipe(fd)==-1)
        die("pipe");

    if ((pid = fork()) == -1)
        die("fork");

    if(pid == 0) {

        dup2 (fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        execl("/usr/bin/base64", "base64", "-d", path, NULL);
        die("execl");

    } else {

        close(fd[1]);
        int temp = read(fd[0], decoded, sizeof(decoded));
        fprintf(fptr, "%.*s\n", temp, decoded);
        wait(NULL);
    }
}
```
Dikarenakan hasil dari ```exec``` tidak dapat langsung diassign dalam sebuah variable, fungsi ini akan menggunakan ```pipe()``` untuk melakukannya. Dengan men-define ```die(e)``` untuk melakukan do while seperti yang tertulis, serta deklarasi variable ```fd``` untuk ```pipe()``` dan ```pid``` untuk melakukan ```fork()```, fungsi akan mengecek apabila ```pipe()``` dan ```fork()``` berhasil. Jika iya, maka pada child-nya, fungsi akan menjalankan fungsi ```dup2(fd[1], STDOUT_FILENO)``` untuk redirect standard output ```(STDOUT_FILENO)``` dan error output ```(STDERR_FILENO)```, lalu close ```fd[0]``` dan ```fd[1]```, kemudian menjalankan ```execl("/usr/bin/base64", "base64", "-d", path, NULL)``` untuk mulai mendecode sesuai path yang dipassing. Setelah itu, menggunakan ```die(execl)```, hasil decode akan di```fprintf``` ke dalam ```stderr```.

Pada parent-nya, fungsi akan close ```fd[1]``` dan read pipe ```fd[0]``` ke variable ```decoded``` dan disimpan dalam variable ```temp```. Setelah itu, menggunakan ```fprintf``` untuk mengoutput hasil ```temp``` dan ```decoded``` ke file pointer ```fptr``` yang dipassing.

Karena decode ini dijalankan secara bersamaan, maka harus dilakukan pemanggilan fungsi ```dcd()``` menggunakan thread pada main:

```bash
    i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&dcd,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
Untuk soal 1c, kedua file ```.txt``` hasil decode tersebut perlu dipindahkan ke directory ```./modul3/hasil/```. Untuk itu, maka digunakan fungsi ```listTXT()```:

```bash
void listTXT(char *basePath){
	make();
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
		mv(src);
      }
    }
    closedir(dir);
}
```
di mana pada awal pemanggilan, fungsi ini akan memanggil fungsi ```make()```:

```bash
void make(){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"mkdir", "/home/kadigas/modul3/hasil", NULL};
			execv("/usr/bin/mkdir", argv);
		}
	else {
			((wait(&status)) > 0);
		}
}
```
untuk membuat directory ```/home/kadigas/modul3/hasil``` terlebih dahulu menggunakan ```execv("/usr/bin/mkdir", argv)```. Setelah itu, dengan menggunakan ```dirent```, dilakukan directory listing pada ```./modul3/``` dengan mengecek kondisi apabila ```dp->d_name``` sekarang bukan ```.```, ```..```, dan pada memiliki ```.txt``` substring-nya. Jika ditemukan, maka variable ```src``` akan mengcopy ```basePath``` yang dipassing yaitu ```./modul3```, ditambahkan dengan ```dp->d_name``` sekarang, dan kemudian dipassing untuk memanggil fungsi ```mv()```:

```bash
void mv(char *path){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"mv", path, "/home/kadigas/modul3/hasil/", NULL};
			execv("/usr/bin/mv", argv);
		}
	else {
			((wait(&status)) > 0);
		}
}
```
di mana fungsi ini akan mengeksekusi ```mv``` untuk memindahkan file dengan ```path``` yang dipassing ke directory ```./modul3/hasil/``` oleh ```execv("/usr/bin/mv", argv)``` . Fungsi ```dcd()``` akan dipanggil di main:

```bash
listTXT("/home/kadigas/modul3/");
```
Untuk soal 1d, kita akan men-zip folder ```./modul3/hasil/``` dengan password yang telah ditentukan, yaitu ```"mihinomenest[user]"``` yang di sini akan ditulis sebagai ```"mihinomenestkadigas"```. Untuk melakukan ini, maka pada main akan memanggil fungsi ```zipping()```:

```bash
zipping("./hasil/");
```
dengan passing ```./hasil/```. Fungsi ```zipping()``` sendiri:

```bash
void zipping(char *path){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"zip", "--password", "mihinomenestkadigas", "-r", "hasil.zip", "-m", path, NULL};
			execv("/usr/bin/zip", argv);
		}
	else {
			((wait(&status)) > 0);
		}
	
	stats = 3;

}
```
Dengan menggunakan ```fork()```, fungsi akan mengeksekusi ```zip``` pada child process-nya menjadi ```hasil.zip``` dengan ```--password``` untuk men-set password yang ditentukan, yaitu ```mihinomenestkadigas```, dengan opsi ```-r``` untuk melakukan zip secara rekursif dalam ```path``` yang dipassing, dan ```-m``` untuk melakukan delete pada directory yang telah di-zip yang dieksekusi oleh ```execv("/usr/bin/zip", argv)``` . Apabila telah selesai, maka ```stats``` akan diset = 3.

Untuk soal 1e, kita diminta untuk mengunzip file ```hasil.zip``` dan membuat file ```no.txt``` dan menuliskan ```No``` di dalamnya secara bersamaan. Untuk melakukannya, maka digunakan lagi pembuatan thread pada main:

```bash
    i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipno,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}

	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
yang akan mengeksekusi fungsi ```unzipno()```. Fungsi ```unzipno()```:

```bash
void* unzipno(void *arg)
{
    while(stats != 3){

    }

    char *argv[] = {"unzip", "-q", "hasil.zip", "-d", "/home/kadigas/modul3/", NULL};

    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0])) //thread untuk listing quote
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/unzip", argv);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1])) // thread untuk listing music
	{
        FILE *fptr;
    	fptr = fopen("/home/kadigas/modul3/no.txt", "w+");
		fprintf(fptr, "No");
		fclose(fptr);
	}

    return NULL;
}
```
akan menunggu hingga ```stats == 3```. Setelah itu, pada thread pertama (```tid[0]```), fungsi akan melakukan ```fork()``` dan mengeksekusi ```unzip``` dengan ```execv("/usr/bin/unzip", argv)```, sedangkan thread kedua (```tid[1]```), fungsi akan membuat dan membuka ```./modul3/no.txt``` dan menggunakan ```fprintf``` untuk menulis ```"No"``` pada file.

Setelah thread selesai dijalankan, main akan memanggil fungsi ```mv()```:

```bash
mv("/home/kadigas/modul3/no.txt");
```
untuk memindahkan file ```no.txt``` yang ada pada ```./modul3/``` ke directory ```./modul3/hasil/``` sehingga file ```no.txt``` akan berada dalam 1 folder ```hasil``` bersama file ```quote.txt``` dan ```music.txt```. Setelah itu, dilakukan:

```bash
zipping("./hasil/");
```
untuk men-zip kembali folder ```hasil``` yang kini berisi 3 file ```.txt```.

Screenshots:
<img src="./screenshot/soal1/01.png" alt="alter message">
<img src="./screenshot/soal1/02.png" alt="alter message">
<img src="./screenshot/soal1/03.png" alt="alter message">
<img src="./screenshot/soal1/04.png" alt="alter message">
<img src="./screenshot/soal1/05.png" alt="alter message">
<img src="./screenshot/soal1/06.png" alt="alter message">

## Soal 2
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. 
#### Client
```bash
// Memilih antara register dan login
void reg_log(){

    char chc[SIZE];
    printf("Enter login/register: ");
    scanf("%s", chc);
    printf("Dont type anything, wait until another client disconnect!\n");
    write(sock, chc, BUFF);
    if(!strcmp(chc, "register"))
    {
       regist();
    }
    else if(!strcmp(chc, "login"))
    {
       login();
    }

}  
```
#### Server
```bash
// Memilih antara register dan login
void reg_log_handler(int new_socket){

   char chc[BUFF];
   valread = read(new_socket, chc, BUFF);
   if(!strcmp(chc, "register"))
   {
       regist_handler(new_socket);
   }
   else if(!strcmp(chc, "login"))
   {
       login_handler(new_socket);
   }

}
```
Sistem register diatur oleh fungsi regist pada client dan regist_handler pada server. Perihal pengecekan ketersediaan id/username dan validasi format password dilakukan di fungsi regist di client.
### Client
```bash
// Register
void regist(){
    
    bool exist=true;
    char id[SIZE]={}, password[SIZE]={}, temp_buff[BUFF]={};
    char id_data[BUFF][SIZE]={};
    int idx, i;
    valread = read(sock, temp_buff, BUFF);
    idx = string_to_int(temp_buff);
    valread = read(sock, id_data, sizeof(id_data));
    printf("Server free, you may type now\n");

    // Mengambil input username
    printf("Username: ");
    while(exist){
        scanf("%s", id);

        // Mengecek apakah username sudah ada
        for (i = 0; i < idx; i++){
            if (!strcmp(id_data[i], id)){
                exist=false;
                break;
            }
        }
        if (!exist){
            exist = true;
            printf("Username already exist\nUsername: ");
        }else {
            exist = false;
            getchar();
        }
    }

    // Mengambil input password
    printf("Password: ");
    int nc, lc, ucc, lcc;
    nc=lc=ucc=lcc=0;
    while(!lc || !nc || !ucc || !lcc)
    {
        scanf("%s", password);
        int len = strlen(password);

        // Mengecek apakah password memenuhi syarat
        for(int i=0; i<len; i++)
        {
            if(isdigit(password[i]) && nc==0)
            {
                nc=1;
            }

            if(isupper(password[i]) && ucc==0)
            {
                ucc=1;
            }

            if(islower(password[i]) && lcc==0)
            {
                lcc=1;
            }

            if(len>=6 && lc==0)
            {
                lc=1;
            }
        }

        if(!lc || !nc || !ucc || !lcc)
        {
            printf("\nPassword Format Invalid, password must contain the following:\n");
            if (!lc) printf("- Minimum 6 characters\n");
            if (!nc) printf("- A  number\n");
            if (!lcc) printf("- A  lowercase letter\n");
            if (!ucc) printf("- An uppercase letter\n");
            nc=lc=ucc=lcc=0;
            memset(password, '\0', SIZE);
            printf("Password: ");
        }

    }
  
    write(sock, id, BUFF);
    write(sock, password, BUFF);
}
```
Proses memasukkan data username dan password ke dalam users.txt dilakukan di fungsi regist_handler di server. 
### Server
```bash
// Register
void regist_handler(int new_socket){

    /* Membuka file users.txt dengan mode a+
     * Supaya bisa melakukan read, write, append dan membuat file baru jika memang belum ada */
    FILE* fptr = fopen(users_file,"a+");
    
    // Memasukkan data id/username dari file users.txt ke dalam array 
    char line[BUFF], id_data[BUFF][SIZE]={}, pwd_data[BUFF][SIZE];
    int idx=0;
   while(!feof(fptr)) {
         fscanf(fptr, "%[^:]:%[^\n]\n", id_data[idx], pwd_data[idx]);
        idx++;
   }
    char temp_buff[BUFF]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, BUFF);
    write(new_socket, id_data, sizeof(id_data));

    // Membaca data id dan password dari client lalu dimasukkan ke file users.txt
    char id[BUFF]={}, 
         pass[BUFF]={};
    valread = read(new_socket, id, BUFF);
    valread = read(new_socket, pass, BUFF);
    fprintf(fptr, "%s:%s\n", id, pass);
    printf("User '%s' successfully registered\n", id);
    fclose(fptr);
    
}
```

Sistem login diatur oleh fungsi login pada client dan login_handler pada server. Pengecekan apakah username dan password yang dimasukkan sudah sesuai dengan yang teregister dilakukan di client.
### Client
```bash
// Login
void login(){

    int status;
    char id[BUFF], password[BUFF], temp_buff[BUFF]={};
    char id_data[BUFF][SIZE], pwd_data[BUFF][SIZE];
    int idx;
    valread = read(sock, temp_buff, BUFF);
    idx = string_to_int(temp_buff);
    read2d(id_data, idx);
    read2d(pwd_data, idx);
  
    printf("Server free, you may type now\n");
    while(!auth_){
        int auth_idx=-1;
        while(auth_idx == -1){
            
            // Dapatkan input id/username
            printf("Username: ");
            scanf("%s", id);

            // Cek apakah id/username sudah terdaftar
            for (int i = 0; i < idx; i++){

                if (!strcmp(id_data[i], id)){
                    auth_idx = i;
                    break;
                }
            }

            if(auth_idx == -1)
            {
                printf("Wrong Username\n");
            }

        }

        // Dapatkan input password
        printf("Password: ");
        scanf("%s", password);

        // Cek apakah password sudah terdaftar
        if (!strcmp(password, pwd_data[auth_idx])){
            strcpy(userlog, id_data[auth_idx]);
            auth_ = 1;
            printf("Login Successful\n");
        }else {
            printf("Wrong Password\n");
        }
    }
    
}
```
### Server
```bash
// Login
void login_handler(int new_socket){

    // Membuka file users.txt dengan mode r+
    FILE* fptr = fopen(users_file,"r+");

    // Memasukkan data id dan password ke dalam array
   char id_data[BUFF][SIZE], pwd_data[BUFF][SIZE];
   int idx=0;
    while(!feof(fptr)) {
       fscanf(fptr, "%[^:]:%[^\n]\n", id_data[idx], pwd_data[idx]);
       idx++;
   } 

    fclose(fptr);
    char temp_buff[BUFF]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, BUFF);
    write2d(id_data, idx, new_socket);
    write2d(pwd_data, idx, new_socket);

}
```
Untuk sistem pemilihan command diatur oleh fungsi command pada client dan command_handler pada server
### Client
```bash
int command(){

    char com[SIZE]={};
    printf("Command list\nadd\nsee\nexit\nCommand: ");
    scanf("%s", com);
    write(sock, com, sizeof(com));

    if (!strcmp(com, "add")){

        char question[4][SIZE]={0};
        char client_dir[SIZE*2];
        getcwd(client_dir, SIZE*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[SIZE]={}, path[SIZE]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[SIZE]={};
                read(sock, temp, SIZE);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, SIZE);
                }
            }
        }
        printf("\n");
        return 1;

    }
    
    else if (!strcmp(com, "see")){

        char author[SIZE], problem[SIZE], buffer[SIZE];
        printf("Problem list:\n");
        valread = read(sock, buffer, SIZE);
        int idx = string_to_int(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, SIZE);
            read(sock, author, SIZE);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;

    }
    
    else if (!strcmp(com, "exit")){
        return 0;
    }else {
        printf("Command %s not available\n\n", com);
        return 1;
    }
}
```
### Server
```bash
int command_handler(int new_socket){
    
    char com[SIZE]={};
    valread = read(new_socket, com, sizeof(com));

    if (!strcmp(com, "add")){

        printf("User '%s' adding a problem\n", userlog);
        FILE* fp = fopen(database, "a+");

        char question[4][SIZE] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);

        char serv_path[SIZE*2], client_path[SIZE*2], prob[SIZE]={};
        read(new_socket, client_path, sizeof(client_path));

        for(int i=0; i<4; i++){
            char buffer[BUFF]={}, src_path[BUFF]={}, dest_path[BUFF]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", SIZE);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", SIZE);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                download_file(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem '%s' has been added by '%s'\n", prob, userlog);
        fclose(fp);
        return 1;

    }

    else if (!strcmp(com, "see"))
    {

        printf("User '%s' see problem list\n", userlog);
        FILE* fp = fopen(database, "r+");
        char author[BUFF][SIZE]={}, problem[BUFF][SIZE]={};

        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, SIZE, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], SIZE);
            write(new_socket, author[i], SIZE);
        }
        fclose(fp);
        return 1;
        
    }
    
    return 0;

} 
```
#### Screenshots:
<img src="./screenshot/soal2/01.png" alt="alter message">
<img src="./screenshot/soal2/02.png" alt="alter message">
<img src="./screenshot/soal2/03.png" alt="alter message">
<img src="./screenshot/soal2/04.png" alt="alter message">
<img src="./screenshot/soal2/05.png" alt="alter message">
<img src="./screenshot/soal2/07.png" alt="alter message">
<img src="./screenshot/soal2/08.png" alt="alter message">
<img src="./screenshot/soal2/09.png" alt="alter message">

## Soal no. 3
Pada soal nomor 3, kita diperintahkan untuk membuat folder sesuai dengan ekstensi dari setiap file yang ada di folder hartakarun. Untuk melakukan hal tersebut, kita menyediakan 3 fungsi penting yaitu

```bash
int isFile(char *path)
void *moveFile(void *filename)
void RecursiveFolder(char *path, int *iteration)
```

Sebelum mengerjakan soal, kita harusnya sudah meng-extract ```hartakarun.zip``` yang diberikan dari modul praktikum dan disimpan di folder ```home/[user]/shift3```. Kalau sudah melakukan hal tersebut maka kita dapat mulai mengerjakan soal dari yang ```3b``` terlebih dahulu karena kita harus mengecek file-file yang ada di folder hartakarun secara rekursif, sehingga dengan itu kita tahu ekstensi dari file-file yang ada.

```bash
int main(){
    int flag,iteration = 0;
    char basePath[1000],path[500],*user;
    void *status;
    
    user = getenv("USER");
    strcpy(path,"/home/");
    strcat(path,user);
    strcat(path,"/shift3/hartakarun");
    strcpy(basePath, path);

    RecursiveFolder(basePath, &iteration);

	....

}
```
pada fungsi ```main()``` di atas kita membuat path ```home/[user]/shift3/hartakarun``` yang mana path ini berisikan file-file hasil extract ```hartakarun.zip```, lalu kita simpan path ini ke variabel ```basePath``` lalu mempassing argumen ```basePath``` dan ```iteration``` ke fungsi ```RecursiveFolder()```

```bash
void RecursiveFolder(char *path, int *iteration){
    char tempPath[1000];
    struct dirent *dirent;
    DIR *dir = opendir(path);

    if(dir != NULL){
        while(dirent = readdir(dir)){
            int pembanding1 = strcmp(dirent->d_name,".");
            int pembanding2 = strcmp(dirent->d_name,"..");
            if(pembanding1 == 0 || pembanding2 == 0)continue;

            strcpy(tempPath, path);
            strcat(tempPath, "/");
            strcat(tempPath, dirent->d_name);

            if(IsFile(tempPath)){
                strcpy(FileKeeper[*iteration], tempPath);
                *iteration = *iteration + 1;
            }
            else RecursiveFolder(tempPath,iteration);
            
        }
        closedir(dir);
    }
    else perror("Tidak dapat membaca folder");
}
```

Pada fungsi ```RecursiveFolder(...)``` kita mencari file-file yang ada di folder path secara rekursif, pada case ini pathnya adalah ```home/[user]/shift3/hartakarun```. Untuk melakukan rekursif, kita dapat melakukannya dengan mengecek apakah yang sedang dibaca oleh mesin itu berupa file atau bukan dengan menggunakan fungsi ```IsFile(...)```

```bash
int IsFile(char *path){
    struct stat sb;
    stat(path, &sb);
    return S_ISREG(sb.st_mode);
}

....

if(IsFile(tempPath)){
	strcpy(FileKeeper[*iteration], tempPath);
	*iteration = *iteration + 1;
}
else RecursiveFolder(tempPath,iteration);
```

Apabila bukan file, maka kemungkinan itu sebuah folder sehingga kita dapat memanggil fungsi ```RecursiveFolder(...)``` dengan path yang berbeda dari sebelumnya. Apabila sebuah file maka file tersebut akan kita simpan di variabel array ```FileKeeper[*iteration]``` dengan indexnya adalah iterasi pada waktu itu.

Setelah kita mendapatkan file-file yang diinginkan, maka kita dapat mengerjakan ```3c``` dan ```3a``` yang mana kita mengkategorikan file yang sudah didapat dengan menggunakan satu thread.

```bash
pthread_t tid[iteration];

    for(int i = 0; i<iteration; i++){
        // printf("%s", FileKeeper[i])
        pthread_create(&tid[i], NULL, MoveFile, (void*)FileKeeper[i]);
        pthread_join(tid[i], &status);
    }
    exit(0);
```

Kita menggunakan thread tersebut untuk menjalankan fungsi ```MoveFile``` dengan parameternya berupa ```FileKeeper[i]``` yang mana merupakan file dengan iterasi ke-```i```.

```bash
void *MoveFile(void *filename){
  char str[999],buffer[999],buffer2[999],buffer3[999],buffer4[999],path[1000],tempDest[1000],cwd[1000],*user,currentPath[500];
  char fileName[1000],fileExt[1000],fileExt2[1000];

  user = getenv("USER");
  strcpy(currentPath,"/home/");
  strcat(currentPath,user);
  strcat(currentPath,"/shift3/hartakarun/");

  strcpy(cwd,currentPath);
  strcpy(path, (char*) filename);
  strcpy(buffer4, path);
  
  ....

}
```
Pada fungsi tersebut kita membuat berbagai variabel penyimpan path agar path yang original masih tetap ada selama memproses pembuatan folder kategori.

```bash
char *fileExt3;
char dot1 = '.';
fileExt3 = strchr(buffer4, dot1);
```

Pada baris tersebut kita mencoba mendapatkan kategori dari file dengan menyimpannya ke variabel ```fileExt3```

```bash
strcpy(buffer, path);
  char *token=strtok(buffer, "/");
  while(token != NULL){
      sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }
```

Pada baris tersebut kita mencoba menghilangkan tanda ```/``` pada path dan disimpan ke variabel ```fileName```

```bash
int countdot = 0;
  char *token2=strtok(buffer2, ".");
  while(token2 != NULL){
      sprintf(fileExt, "%s", token2);
      token2=strtok(NULL, ".");
      countdot++;
  }
```

Pada baris tersebut kita mencoba untuk menghitung berapa banyak ```.``` pada sebuah file dan disimpan ke variabel ```fileExt```

```bash
char dot = '.';
  char first = buffer3[0];
  
  if(dot == first)strcpy(fileExt, "hidden");
  else if(countdot >= 3){
      strcpy(fileExt, fileExt3+1);
    //   printf("fileExt %s\n",fileExt);
  }
  else if (countdot <=1 )strcpy(fileExt, "unknown");
```

Pada baris ini kita mencoba untuk menyimpan directory lewat variabel ```fileExt``` sesuai dengan kategori file yang sedang dibaca. directory ```unknown``` untuk file yang tidak ada ekstensi, directory ```hidden``` untuk file yang hidden, dan directory sesuai dengan ekstensi dari filenya.

```bash
for (int i = 0; i < sizeof(fileExt); i++)fileExt[i] = tolower(fileExt[i]);
```

Pada baris tersebut kita menjadikan kategori yang ada agar tidak case sensitive dengan menjadikannya huruf kecil semua.

```bash
strcat(currentPath,fileExt);
mkdir(currentPath, 0777);
```

Pada baris ini kita membuat folder yang sudah diproses sesuai dengan kategori file yang sedang dibaca

```bash
strcat(cwd, "/");
strcat(cwd,fileExt);
strcat(cwd, "/");
strcat(cwd, fileName);
strcpy(tempDest, cwd);

rename(buffer, tempDest);
// printf("buffer %s\n",buffer);
return (void *) 1;
```

Pada baris ini kita memberikan penamaan yang baru untuk semua file-file yang ada sesuai dengan ekstensinya

Setelah membuat folder sesuai dengan kategori file-file yang ada, maka kita dapat mengerjakan ```3d``` dan ```3e``` yang mana kita menggunakan socket untuk server-client

### Server

```bash
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

int main(){
  char *file_name = "hartakarun.zip";
  char buffer[BUFSIZ];
  int client_fd,filestream,server_fd;
  int enable_reuseaddr = 1;

  socklen_t client_len;
  ssize_t read_status;
  struct protoent *proto;
  struct sockaddr_in client_address, server_address;
  unsigned short port = 7702u;

  // Create a socket and listen to it.
  proto = getprotobyname("tcp");
  if (proto == NULL) {
    perror("getprotobyname");
    exit(EXIT_FAILURE);
  }
  server_fd = socket(AF_INET, SOCK_STREAM, proto->p_proto);
  if (server_fd == -1) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr, sizeof(enable_reuseaddr)) < 0) {
    perror("setsockopt(SO_REUSEADDR) failed");
    exit(EXIT_FAILURE);
  }
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(port);

  if (bind(server_fd, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
    perror("bind");
    exit(EXIT_FAILURE);
  }
  if (listen(server_fd, 5) == -1) {
    perror("listen");
    exit(EXIT_FAILURE);
  }
  fprintf(stderr, "listening on port %d\n", port);

  while(1) {
    client_len = sizeof(client_address);
    printf("Menunggu Client\n");

    client_fd = accept(server_fd, (struct sockaddr*)&client_address, &client_len);
    filestream = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);

    if (filestream == -1) {
      perror("open");
      exit(EXIT_FAILURE);
    }
    do {
      read_status = read(client_fd, buffer, BUFSIZ);
      if (read_status == -1) {
        perror("read");
        exit(EXIT_FAILURE);
      }
      if (write(filestream, buffer, read_status) == -1) {
        perror("write");
        exit(EXIT_FAILURE);
      }
    } while (read_status > 0);
    close(filestream);
    close(client_fd);
  }
  exit(EXIT_SUCCESS);
}
```

### Client

```bash
#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

void* zip_file() {
    char path[500],*user;
    user = getenv("USER");
    strcpy(path,"/home/");
    strcat(path,user);
    strcat(path,"/shift3/hartakarun");

    pid_t child_id;
    int status1;
        
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"zip", "-q", "-r", "hartakarun.zip", path, NULL};
        execv("/usr/bin/zip", argv);
    } else while (wait(&status1) > 0);
}

int main(int argc, char *argv[]){
  char *file_name = "hartakarun.zip";
  char *hostname = "127.0.0.1";
  char buffer[BUFSIZ];
  in_addr_t in_address;
  int filestream,sock;
  ssize_t read_status;
  struct hostent *host;
  struct protoent *proto;
  struct sockaddr_in socket_address;
  unsigned short port = 7702;

  char cmd[100];

  scanf("%[^\n]s", cmd);

  if(strcmp(cmd, "send hartakarun.zip") != 0)exit(EXIT_FAILURE);
  zip_file();
  if (argc > 1) {
    if (strcmp(argv[1], "send") == 0) {
      if (argc > 2) file_name = argv[2];
      else exit(EXIT_FAILURE);
    } 
    else exit(EXIT_FAILURE);
  }

  filestream = open(file_name, O_RDONLY);

  if (filestream == -1) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  // Get socket.
  proto = getprotobyname("tcp");
  if (proto == NULL) {
    perror("getprotobyname");
    exit(EXIT_FAILURE);
  }
  sock = socket(AF_INET, SOCK_STREAM, proto->p_proto);
  if (sock == -1) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
  // Prepare socket_address.
  host = gethostbyname(hostname);
  if (host == NULL) {
    fprintf(stderr, "error: gethostbyname(\"%s\")\n", hostname);
    exit(EXIT_FAILURE);
  }
  in_address = inet_addr(inet_ntoa(*(struct in_addr*)*(host->h_addr_list)));
  if (in_address == (in_addr_t)-1) {
    fprintf(stderr, "error: inet_addr(\"%s\")\n", *(host->h_addr_list));
    exit(EXIT_FAILURE);
  }
  socket_address.sin_addr.s_addr = in_address;
  socket_address.sin_family = AF_INET;
  socket_address.sin_port = htons(port);
  // Do the actual connection.
  if (connect(sock, (struct sockaddr*)&socket_address, sizeof(socket_address)) == -1) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  while (1) {
    read_status = read(filestream, buffer, BUFSIZ);
    if (read_status == 0) break;
    if (read_status == -1) {
      perror("read");
      exit(EXIT_FAILURE);
    }
    if (write(sock, buffer, read_status) == -1) {
      perror("write");
      exit(EXIT_FAILURE);
    }
  }
  close(filestream);
  exit(EXIT_SUCCESS);
}

```

Untuk mengerjakan soal tersebut kita mulai dengan ```3e``` yang mana user mengetikkan command ```send hartakarun.zip``` sehingga sistem akan menjalankan fungsi ```zip_file()```

```bash
void* zip_file() {
    char path[500],*user;
    user = getenv("USER");
    strcpy(path,"/home/");
    strcat(path,user);
    strcat(path,"/shift3/hartakarun");

    pid_t child_id;
    int status1;
        
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"zip", "-q", "-r", "hartakarun.zip", path, NULL};
        execv("/usr/bin/zip", argv);
    } else while (wait(&status1) > 0);
}
```

Di sini kita menggunakan ```fork-exec``` untuk membuat ```hartakarun.zip```. Apabila berhasil mengzip folder tersebut, maka ```hartakarun.zip``` akan berada di folder yang sama dengan ```client.c``` dan ```server.c```
