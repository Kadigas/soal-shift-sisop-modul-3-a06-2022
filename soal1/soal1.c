#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/wait.h>

#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);

pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;
int stats = 0;

void* dl(void *arg)
{
    char *argv1[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "/home/kadigas/modul3/quote.zip", NULL};
    char *argv2[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "/home/kadigas//modul3/music.zip", NULL};
	
    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0])) //thread untuk download quote
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/wget", argv1);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1])) // thread untuk download music
	{
        child = fork();
        if (child==0) {
		    execv("/usr/bin/wget", argv2);
	    }
        else {
		    ((wait(&status)) > 0);
        }
	}

    stats = 1;

	return NULL;
}
//Referensi: https://phoenixnap.com/kb/wget-command-with-examples

void* unzipping(void *arg)
{
    while(stats != 1){

    }

	char *argv1[] = {"unzip", "-q", "quote.zip", "-d", "/home/kadigas/modul3/quote", NULL};
	char *argv2[] = {"unzip", "-q", "music.zip", "-d", "/home/kadigas/modul3/music", NULL};
	
	pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0])) //thread untuk unzip quote
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/unzip", argv1);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1])) // thread untuk unzip music
	{
        child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv2);
	    }
        else {
		    ((wait(&status)) > 0);
        }
	}

    stats = 2;

	return NULL;
}
//Referensi: https://stackoverflow.com/questions/14767650/c-unzip-returns-cannot-create-extraction-directory

void decode(char *path, FILE *fptr){
    int fd[2];
    pid_t pid;
    char decoded[4096];
    
    if (pipe(fd)==-1)
        die("pipe");

    if ((pid = fork()) == -1)
        die("fork");

    if(pid == 0) {

        dup2 (fd[1], STDOUT_FILENO);
        close(fd[0]);
        close(fd[1]);
        execl("/usr/bin/base64", "base64", "-d", path, NULL);
        die("execl");

    } else {

        close(fd[1]);
        int temp = read(fd[0], decoded, sizeof(decoded));
        fprintf(fptr, "%.*s\n", temp, decoded);
        wait(NULL);
    }
}
//Referensi: https://stackoverflow.com/questions/7292642/grabbing-output-from-exec

void list(char *basePath){
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul3/quote.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        decode(src, fptr);
      }
    }

    fclose(fptr);
    closedir(dir);
}

void list2(char *basePath){
    FILE *fptr;
    fptr = fopen("/home/kadigas/modul3/music.txt", "w+");
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
        decode(src, fptr);
      }
    }

    fclose(fptr);
    closedir(dir);
}

void* dcd(void *arg)
{
    while(stats != 2){

    }

    char path1[] = "/home/kadigas/modul3/quote/";
	char path2[] = "/home/kadigas/modul3/music/";

    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0])) //thread untuk listing quote
	{
		list(path1);
    }

	else if(pthread_equal(id,tid[1])) // thread untuk listing music
	{
        list2(path2);
	}
	
	return NULL;
}

void make(){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"mkdir", "/home/kadigas/modul3/hasil", NULL};
			execv("/usr/bin/mkdir", argv);
		}
	else {
			((wait(&status)) > 0);
		}
}

void mv(char *path){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"mv", path, "/home/kadigas/modul3/hasil/", NULL};
			execv("/usr/bin/mv", argv);
		}
	else {
			((wait(&status)) > 0);
		}

//Referensi: https://www.ibm.com/docs/en/aix/7.1?topic=m-mv-command
}

void listTXT(char *basePath){
	make();
    char path[1000];
    struct dirent *dp;
    char src[100];
    DIR *dir = opendir(basePath);
    char list[100];

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".txt")) 
      {
        strcpy(src, basePath);
        strcat(src, dp->d_name);
		mv(src);
      }
    }
    closedir(dir);
}

void zipping(char *path){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv[] = {"zip", "--password", "mihinomenestkadigas", "-r", "hasil.zip", "-m", path, NULL};
			execv("/usr/bin/zip", argv);
		}
	else {
			((wait(&status)) > 0);
		}
	
	stats = 3;

//Referensi: 
// https://stackoverflow.com/questions/32302705/zip-error-invalid-command-arguments-cannot-write-zip-file-to-terminal
// https://www.geeksforgeeks.org/zip-command-in-linux-with-examples/
}

void zipno(){
	int status;
	pid_t child_id = fork();
	
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
		
	if (child_id == 0) {
			char *argv2[] = {"zip", "--password", "mihinomenestkadigas", "hasil.zip", "-m", "./no.txt", NULL};
			execv("/usr/bin/zip", argv2);
		}
	else {
			((wait(&status)) > 0);
		}
}

void* unzipno(void *arg)
{
    while(stats != 3){

    }

    char *argv[] = {"unzip", "-q", "hasil.zip", "-d", "/home/kadigas/modul3/", NULL};

    pthread_t id=pthread_self();
	int status;
	if(pthread_equal(id,tid[0])) //thread untuk listing quote
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/unzip", argv);
	    }
        else {
		    ((wait(&status)) > 0);
        }
    }

	else if(pthread_equal(id,tid[1])) // thread untuk listing music
	{
        FILE *fptr;
    	fptr = fopen("/home/kadigas/modul3/no.txt", "w+");
		fprintf(fptr, "No");
		fclose(fptr);
	}

    return NULL;
}

int main(void)
{
	int i=0;
	int err;
    while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&dl,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
    
    i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipping,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&dcd,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	listTXT("/home/kadigas/modul3/");
	zipping("./hasil/");

	i=0;

	while(i<2)
	{
        err=pthread_create(&(tid[i]),NULL,&unzipno,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		i++;
	}

	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	mv("/home/kadigas/modul3/no.txt");
	zipping("./hasil/");

	return 0;
}