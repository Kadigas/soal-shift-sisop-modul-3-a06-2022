#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <limits.h>


pthread_t tid[500];
char FileKeeper[2000][2000];

//Check kalau pathnya berupa file atau tidak
int IsFile(char *path){
    struct stat sb;
    stat(path, &sb);
    return S_ISREG(sb.st_mode);
}

void *MoveFile(void *filename){
  char str[999],buffer[999],buffer2[999],buffer3[999],buffer4[999],path[1000],tempDest[1000],cwd[1000],*user,currentPath[500];
  char fileName[1000],fileExt[1000],fileExt2[1000];

  user = getenv("USER");
  strcpy(currentPath,"/home/");
  strcat(currentPath,user);
  strcat(currentPath,"/shift3/hartakarun/");

  strcpy(cwd,currentPath);
  strcpy(path, (char*) filename);
  strcpy(buffer4, path);

  char *fileExt3;
  char dot1 = '.';
  fileExt3 = strchr(buffer4, dot1);
  // printf("%s", fileExt3);

  strcpy(buffer, path);
  char *token=strtok(buffer, "/");
  while(token != NULL){
      sprintf(fileName, "%s", token);
      token = strtok(NULL, "/");
  }

  strcpy(buffer, path);
  strcpy(buffer2, fileName);
  strcpy(buffer3, fileName);
  // strcpy(buffer4, fileName);

  int countdot = 0;
  char *token2=strtok(buffer2, ".");
  while(token2 != NULL){
      sprintf(fileExt, "%s", token2);
      token2=strtok(NULL, ".");
      countdot++;
  }

  char dot = '.';
  char first = buffer3[0];

  if(dot == first)strcpy(fileExt, "hidden");
  else if(countdot >= 3){
      strcpy(fileExt, fileExt3+1);
    //   printf("fileExt %s\n",fileExt);
  }
  else if (countdot <=1 )strcpy(fileExt, "unknown");
  
  for (int i = 0; i < sizeof(fileExt); i++)fileExt[i] = tolower(fileExt[i]);

  strcpy(buffer, (char*) filename);
  
  strcat(currentPath,fileExt);
//   printf("currentPath %s\n",currentPath);
  mkdir(currentPath, 0777);

  strcat(cwd, "/");
  strcat(cwd,fileExt);
  strcat(cwd, "/");
  strcat(cwd, fileName);
  strcpy(tempDest, cwd);

  rename(buffer, tempDest);
    // printf("buffer %s\n",buffer);
  return (void *) 1;

}

void RecursiveFolder(char *path, int *iteration){
    char tempPath[1000];
    struct dirent *dirent;
    DIR *dir = opendir(path);

    if(dir != NULL){
        while(dirent = readdir(dir)){
            int pembanding1 = strcmp(dirent->d_name,".");
            int pembanding2 = strcmp(dirent->d_name,"..");
            if(pembanding1 == 0 || pembanding2 == 0)continue;

            strcpy(tempPath, path);
            strcat(tempPath, "/");
            strcat(tempPath, dirent->d_name);

            if(IsFile(tempPath)){
                strcpy(FileKeeper[*iteration], tempPath);
                *iteration = *iteration + 1;
            }
            else RecursiveFolder(tempPath,iteration);
            
        }
        closedir(dir);
    }
    else perror("Tidak dapat membaca folder");
}

int main() {

    int flag,iteration = 0;
    char basePath[1000],path[500],*user;
    void *status;
    
    user = getenv("USER");
    strcpy(path,"/home/");
    strcat(path,user);
    strcat(path,"/shift3/hartakarun");
    strcpy(basePath, path);

    RecursiveFolder(basePath, &iteration);

    pthread_t tid[iteration];

    for(int i = 0; i<iteration; i++){
        // printf("%s", FileKeeper[i])
        pthread_create(&tid[i], NULL, MoveFile, (void*)FileKeeper[i]);
        pthread_join(tid[i], &status);
    }
    exit(0);
} 

//REFERENSI
/*
https://www.geeksforgeeks.org/sort-command-linuxunix-examples/
https://www.geeksforgeeks.org/create-directoryfolder-cc-program/
https://stackoverflow.com/questions/40163270/what-is-s-isreg-and-what-does-it-do
https://stackoverflow.com/questions/7430248/creating-a-new-directory-in-c
*/